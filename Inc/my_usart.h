
#ifndef __MY_USART_H
#define __MY_USART_H

#include "stm32f3xx.h"

#define USART_TXEIE_SET     SET_BIT(this->uart->instance->CR1, USART_CR1_TXEIE)
#define USART_TXEIE_RESET   CLEAR_BIT(this->uart->instance->CR1, USART_CR1_TXEIE)

#define USART_RXNEIE_SET     SET_BIT(this->uart->instance->CR1, USART_CR1_RXNEIE)
#define USART_RXNEIE_RESET   CLEAR_BIT(this->uart->instance->CR1, USART_CR1_RXNEIE)

#define RCC_AHBENR_Offset_Msk  (0x14UL << 8)
#define RCC_APB2ENR_Offset_Msk (0x18UL << 8)
#define RCC_APB1ENR_Offset_Msk (0x1CUL << 8)

#define USART_CR1_Offset_Msk  (0x0000U << 8)
#define USART_CR2_Offset_Msk (0x0004U << 8)
#define USART_CR3_Offset_Msk (0x0008U << 8)

typedef enum 
{
  STATE_READY = 0,
  STATE_BUSY = 1
}
State;

typedef enum 
{
  RESULT_OK = 0,
  RESULT_BUSY = 1,
  RESULT_NOT_SUPPORTED = 2,
  RESULT_TIMEOUT = 3,
  RESULT_STOP = 4
}
UsartResult;

typedef enum 
{
  WORD_LENGTH_8 = 0U,
  WORD_LENGTH_9 = 1U
}
UsartWordLength;

typedef enum 
{
  OVERSAMPLING_16 = 0U,
  OVERSAMPLING_8 = 1U
}
UsartOversamplingMode;

typedef enum
{
  // CR1
  USART_IDLEIE = USART_CR1_Offset_Msk | USART_CR1_IDLEIE_Pos,
  USART_RXNEIE = USART_CR1_Offset_Msk | USART_CR1_RXNEIE_Pos,
  USART_TCIE =   USART_CR1_Offset_Msk | USART_CR1_TCIE_Pos,
  USART_TXEIE =  USART_CR1_Offset_Msk | USART_CR1_TXEIE_Pos,
  USART_PEIE =   USART_CR1_Offset_Msk | USART_CR1_PEIE_Pos,
  USART_CMIE =   USART_CR1_Offset_Msk | USART_CR1_CMIE_Pos,
  USART_RTOIE =  USART_CR1_Offset_Msk | USART_CR1_RTOIE_Pos,
  USART_EOBIE =  USART_CR1_Offset_Msk | USART_CR1_EOBIE_Pos,
  // CR2
  USART_LBDIE = USART_CR2_Offset_Msk | USART_CR2_LBDIE_Pos,
  // CR3
  USART_EIE =   USART_CR3_Offset_Msk | USART_CR3_EIE_Pos,
  USART_CTSIE = USART_CR3_Offset_Msk | USART_CR3_CTSIE_Pos,
  USART_WUFIE = USART_CR3_Offset_Msk | USART_CR3_WUFIE_Pos,
}
UsartInterrupt;

typedef struct
{
  UsartWordLength wordLength;
  UsartOversamplingMode overMode;
  uint32_t divider;
  uint32_t timeoutDefault;
  uint32_t timeoutReceive;
  volatile uint32_t *timeoutCounter;
}
UsartConfig;

typedef struct
{
  GPIO_TypeDef *gpio;
  uint8_t pin;
  uint8_t af;
  uint16_t clockMask;
}
GPIO_Pin_Info;

typedef struct
{
  uint16_t clockMask;
  DMA_TypeDef *dma;
  DMA_Channel_TypeDef *dmaChannel;
  IRQn_Type interruptVector;
}
DMA_Channel_Info;

typedef struct
{
  USART_TypeDef *instance;
  uint16_t clockMask;
  GPIO_Pin_Info rxInfo;
  GPIO_Pin_Info txInfo;
  DMA_Channel_Info dmaRx;
  DMA_Channel_Info dmaTx;
  IRQn_Type interruptVector;
}
USART_Info;


class My_USART
{
private:
  const USART_Info *uart;
  
  volatile uint8_t *rxBufferPtr;
  volatile uint8_t *txBufferPtr;
  
  uint8_t rxBufferLength;
  uint8_t txBufferLength;
  
  uint8_t rxEndChar;
  uint8_t txEndChar;
  
  volatile State rxState;
  volatile State txState;
  
  uint32_t circCounter;
  
  volatile uint32_t *timeoutCounter;
  uint32_t timeoutDefault;
  uint32_t receiveStartTime;
  uint32_t receiveStartTimeout;
  
  void initGpio(GPIO_TypeDef *gpio, uint8_t pin, uint8_t af);
  
  void enableRccByMask(uint16_t mask);
  void setBit(uint32_t volatile *reg, uint32_t val, bool x);
  void setCr1Bit(uint32_t val, bool x);
  
  void timeoutValidate(uint32_t *timeout);
  UsartResult waitFlagTimeout(uint32_t flag, uint8_t val, uint32_t timeout);
  
  UsartResult receiveData(void);
  UsartResult transmitData(void);
  
  static const USART_Info usarts[];

public:
  My_USART(uint8_t i, UsartConfig config);
  void enableInterrupt(UsartInterrupt x);
  void disableInterrupt(UsartInterrupt x);
  void initDmaRx(void);
  void initDmaTx(void);
  UsartResult configurate(UsartConfig config);
  UsartResult transmit(uint8_t *buf, uint32_t length, uint8_t endChar = '\0', uint32_t timeout = 0);
  UsartResult transmitDma(uint8_t *buf, uint32_t length, uint8_t endChar = '\0');
  UsartResult transmitIt(uint8_t *buf, uint32_t length, uint8_t endChar = '\0');
  UsartResult receive(uint8_t *buf, uint32_t length, uint8_t endChar = '\n', uint32_t timeout = 0);
  UsartResult receiveDma(uint8_t *buf, uint32_t length, uint8_t endChar = '\n', bool circ = false);
  UsartResult receiveIt(uint8_t *buf, uint32_t length, uint8_t endChar = '\n');
  UsartResult nextReceivedByte(uint8_t *symb);
  
  void interruptHandler(void);
};

#endif /* __MY_USART_H */