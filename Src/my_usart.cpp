
#include "my_usart.h"

const USART_Info My_USART::usarts[] = {
  { 
    /* <USART Base Address>, <RCC Mask> */
    USART1, RCC_APB2ENR_Offset_Msk | RCC_APB2ENR_USART1EN_Pos, 
    /* RX PIN: <GPIO Base Address>, <Pin>, <AF>, <RCC Mask> */
    { GPIOA, 10, 7, RCC_AHBENR_Offset_Msk | RCC_AHBENR_GPIOAEN_Pos }, 
    /* TX PIN */
    { GPIOA, 9,  7, RCC_AHBENR_Offset_Msk | RCC_AHBENR_GPIOAEN_Pos }, 
    /* DMA RX: <RCC Mask>, <DMA Base Address>, <DMA Channel Base Address>, <DMA Channel IRQn> */
    { RCC_AHBENR_Offset_Msk | RCC_AHBENR_DMA1EN_Pos, DMA1, DMA1_Channel5, DMA1_Channel5_IRQn }, 
    /* DMA TX */
    { RCC_AHBENR_Offset_Msk | RCC_AHBENR_DMA1EN_Pos, DMA1, DMA1_Channel4, DMA1_Channel4_IRQn }, 
    /* <USART IRQn> */
    USART1_IRQn 
  },
  { 
    USART2, RCC_APB1ENR_Offset_Msk | RCC_APB1ENR_USART2EN_Pos, 
    { GPIOA, 3,  7, RCC_AHBENR_Offset_Msk | RCC_AHBENR_GPIOAEN_Pos }, 
    { GPIOA, 2,  7, RCC_AHBENR_Offset_Msk | RCC_AHBENR_GPIOAEN_Pos }, 
    { RCC_AHBENR_Offset_Msk | RCC_AHBENR_DMA1EN_Pos, DMA1, DMA1_Channel6, DMA1_Channel6_IRQn }, 
    { RCC_AHBENR_Offset_Msk | RCC_AHBENR_DMA1EN_Pos, DMA1, DMA1_Channel7, DMA1_Channel7_IRQn }, 
    USART2_IRQn 
  },
  { 
    USART3, RCC_APB1ENR_Offset_Msk | RCC_APB1ENR_USART3EN_Pos, 
    { GPIOB, 11, 7, RCC_AHBENR_Offset_Msk | RCC_AHBENR_GPIOBEN_Pos }, 
    { GPIOB, 10, 7, RCC_AHBENR_Offset_Msk | RCC_AHBENR_GPIOBEN_Pos }, 
    { RCC_AHBENR_Offset_Msk | RCC_AHBENR_DMA1EN_Pos, DMA1, DMA1_Channel3, DMA1_Channel3_IRQn }, 
    { RCC_AHBENR_Offset_Msk | RCC_AHBENR_DMA1EN_Pos, DMA1, DMA1_Channel2, DMA1_Channel2_IRQn }, 
    USART3_IRQn 
  },
  { 
    UART4,  RCC_APB1ENR_Offset_Msk | RCC_APB1ENR_UART4EN_Pos,  
    { GPIOC, 11, 5, RCC_AHBENR_Offset_Msk | RCC_AHBENR_GPIOCEN_Pos }, 
    { GPIOC, 10, 5, RCC_AHBENR_Offset_Msk | RCC_AHBENR_GPIOCEN_Pos }, 
    { RCC_AHBENR_Offset_Msk | RCC_AHBENR_DMA2EN_Pos, DMA2, DMA2_Channel3, DMA2_Channel3_IRQn }, 
    { RCC_AHBENR_Offset_Msk | RCC_AHBENR_DMA2EN_Pos, DMA2, DMA2_Channel5, DMA2_Channel5_IRQn }, 
    UART4_IRQn 
  },
  { 
    UART5,  RCC_APB1ENR_Offset_Msk | RCC_APB1ENR_UART5EN_Pos,  
    { GPIOD, 2,  5, RCC_AHBENR_Offset_Msk | RCC_AHBENR_GPIODEN_Pos }, 
    { GPIOC, 12, 5, RCC_AHBENR_Offset_Msk | RCC_AHBENR_GPIOCEN_Pos }, 
    { 0 }, 
    { 0 }, 
    UART5_IRQn 
  }
};


My_USART::My_USART(uint8_t i, UsartConfig config)
{
  if (i > 5)
  {
    i = 1;
  }
  
  this->uart = &(My_USART::usarts[i - 1]);
  
  enableRccByMask(this->uart->clockMask); // UART4 clock
  enableRccByMask(this->uart->rxInfo.clockMask); // GPIO clock
  enableRccByMask(this->uart->txInfo.clockMask); // GPIO clock
  
  initGpio(this->uart->rxInfo.gpio, this->uart->rxInfo.pin, this->uart->rxInfo.af);
  initGpio(this->uart->txInfo.gpio, this->uart->txInfo.pin, this->uart->txInfo.af);
  
  this->timeoutCounter = &(SysTick->VAL);
  
  this->rxState = STATE_READY;
  this->txState = STATE_READY;
  
  NVIC_SetPriority(this->uart->interruptVector, 0);
  NVIC_EnableIRQ(this->uart->interruptVector);
  
  this->configurate(config);
}

/**
  * @brief  Enables clocking by mask (sets bit in RCC register).
  * @param  mask        0xFF00 - RCC register offset, 0x00FF - bit offset
  * @retval None
  */
void My_USART::enableRccByMask(uint16_t mask)
{
  SET_BIT(*((volatile uint32_t *)(RCC_BASE + (mask >> 8U))), (1 << (mask & 0xFFU)));
}

/**
  * @brief  Configures the USART.
  * @param  config      UsartConfig
  * @retval None
  */
UsartResult My_USART::configurate(UsartConfig config)
{
  if (this->rxState != STATE_READY || this->txState != STATE_READY)
  {
    return RESULT_BUSY;
  }
  
  this->timeoutDefault = config.timeoutDefault;
  this->receiveStartTimeout = config.timeoutReceive;
  this->timeoutCounter = config.timeoutCounter;
  
  this->setCr1Bit(USART_CR1_TE | USART_CR1_RE, false);
  this->setCr1Bit(USART_CR1_UE, false);
  
  this->setCr1Bit(USART_CR1_M0, config.wordLength);
  this->setCr1Bit(USART_CR1_OVER8, config.overMode);
  
  if (config.overMode == OVERSAMPLING_16)
  {
    this->uart->instance->BRR = config.divider;
  }
  else
  {
    this->uart->instance->BRR = ((config.divider & 0x0FUL) >> 1) | (config.divider & ~(0x0FUL));
  }
  
  this->setCr1Bit(USART_CR1_TE | USART_CR1_RE, true);
  this->setCr1Bit(USART_CR1_UE, true);
  
  return RESULT_OK;
}

/**
  * @brief  Configures the pin.
  * @param  gpio        Port base address
  * @param  pin         Pin
  * @param  af          Alternate function number
  * @retval None
  */
void My_USART::initGpio(GPIO_TypeDef *gpio, uint8_t pin, uint8_t af)
{
  MODIFY_REG(gpio->MODER,  (1UL << (pin * 2)), (1UL << (pin * 2 + 1))); // alternate function mode
  MODIFY_REG(gpio->OTYPER, (1UL << pin), 0);
  MODIFY_REG(gpio->PUPDR,  (1UL << (pin * 2 + 1)), (1UL << (pin * 2))); // pull-up
  SET_BIT(gpio->OSPEEDR, (1UL << (pin * 2)) | (1UL << (pin * 2 + 1))); // maximum speed
  SET_BIT(gpio->AFR[pin >> 3], uint32_t(af) << ((pin & 0x07U) * 4));   // alternate function select
}

/**
  * @brief  Enables TX DMA clocking and interrupt.
  * @retval None
  */
void My_USART::initDmaTx(void)
{
  enableRccByMask(this->uart->dmaTx.clockMask); // clock DMA
  
  NVIC_SetPriority(this->uart->dmaTx.interruptVector, 0);
  NVIC_EnableIRQ(this->uart->dmaTx.interruptVector);
}

/**
  * @brief  Enables RX DMA clocking and interrupt.
  * @retval None
  */
void My_USART::initDmaRx(void)
{
  enableRccByMask(this->uart->dmaRx.clockMask); // clock DMA
  
  NVIC_SetPriority(this->uart->dmaRx.interruptVector, 0);
  NVIC_EnableIRQ(this->uart->dmaRx.interruptVector);
}

/**
  * @brief  Enable specified interrupt.
  * @param  x   UsartInterrupt
  * @retval None
  */
void My_USART::enableInterrupt(UsartInterrupt x)
{
  SET_BIT(*(&(this->uart->instance->CR1) + (x >> 8)), (1UL << (x & 0xFF)));
}

/**
  * @brief  Disable specified interrupt.
  * @param  x   UsartInterrupt
  * @retval None
  */
void My_USART::disableInterrupt(UsartInterrupt x)
{
  CLEAR_BIT(*(&(this->uart->instance->CR1) + (x >> 8)), (1UL << (x & 0xFF)));
}

/**
  * @brief  Set the Bit for the specified register in specified value.
  * @param  reg         Register pointer
  * @param  val         Bit
  * @param  x           If true, then Bit will be set, else reset
  * @retval None
  */
void My_USART::setBit(uint32_t volatile *reg, uint32_t val, bool x)
{
  if (x)
  {
    SET_BIT(*reg, val);
  }
  else
  {
    CLEAR_BIT(*reg, val);
  }
}

/**
  * @brief  Set CR1 bit.
  * @param  val         Bit
  * @param  x           If true, then Bit will be set, else reset
  * @retval None
  */
void My_USART::setCr1Bit(uint32_t val, bool x)
{
  this->setBit(&(this->uart->instance->CR1), val, x);
}

/**
  * @brief  If timeout is zero, then set timeout to default value.
  * @param  timeout     Timeout
  * @retval None
  */
void My_USART::timeoutValidate(uint32_t *timeout)
{
  if (*timeout == 0)
  {
    *timeout = this->timeoutDefault;
  }
}

/**
  * @brief  Wait until the flag is not set to the specified value.
  * @param  flag        Flag
  * @param  val         Desired value
  * @param  timeout     Timeout of waiting
  * @retval UsartResult
  */
UsartResult My_USART::waitFlagTimeout(uint32_t flag, uint8_t val, uint32_t timeout)
{
  uint32_t startTime = *(this->timeoutCounter);
  
  while ((startTime + timeout > *(this->timeoutCounter)) && 
         (this->receiveStartTime + this->receiveStartTimeout > *(this->timeoutCounter)))
  {
    if (((this->uart->instance->ISR & flag) == flag ? SET : RESET) == val)
    {
      return RESULT_OK;
    }
    timeout -= 1;
  }
  
  return RESULT_TIMEOUT;
}

/**
  * @brief  Reading data from RDR to buffer.
  * @retval UsartResult
  */
UsartResult My_USART::receiveData(void)
{
    *(this->rxBufferPtr) = this->uart->instance->RDR;
    
    if ((this->rxEndChar != 0xFF) && (*(this->rxBufferPtr) == this->rxEndChar))
    {
      this->rxBufferLength = 0;
      return RESULT_STOP;
    }
    
    this->rxBufferPtr += 1;
    
    if (--(this->rxBufferLength) == 0)
    {
      return RESULT_STOP;
    }
    
    return RESULT_OK;
}

/**
  * @brief  Writing data to TDR from buffer.
  * @retval UsartResult
  */
UsartResult My_USART::transmitData(void)
{
  this->uart->instance->TDR = *(this->txBufferPtr);
  
  this->txBufferPtr += 1;
  
  if ((this->txEndChar != 0xFF) && (*(this->txBufferPtr) == this->txEndChar))
  {
    this->txBufferLength = 0;
    return RESULT_STOP;
  }
  
  if (--(this->txBufferLength) == 0)
  {
    return RESULT_STOP;
  }
  
  return RESULT_OK;
}

/**
  * @brief  Start blocking transmission.
  * @param  buf         Buffer address
  * @param  length      Length of Buffer
  * @param  endChar     End of data character
  * @param  timeout     Timeout from the last transmitted character
  * @retval UsartResult
  */
UsartResult My_USART::transmit(uint8_t *buf, uint32_t length, uint8_t endChar, uint32_t timeout)
{
  if (this->txState != STATE_READY)
  {
    return RESULT_BUSY;
  }
    
  this->timeoutValidate(&timeout);
  
  this->txState = STATE_BUSY;
  
  this->txBufferPtr = buf;
  this->txBufferLength = length;
  this->txEndChar = endChar;
  
  UsartResult result = RESULT_OK;

  while (result == RESULT_OK)
  {
    result = waitFlagTimeout(USART_ISR_TXE, SET, timeout);
    if (result != RESULT_OK)
    {
      this->txState = STATE_READY;
      return result;
    }
    
    result = this->transmitData();
  }
  
  this->txState = STATE_READY;
  
  return RESULT_OK;
}

/**
  * @brief  Start non-blocking transmission using TXE interrupt.
  * @param  buf         Buffer address
  * @param  length      Length of Buffer
  * @param  endChar     End of data character
  * @retval UsartResult
  */
UsartResult My_USART::transmitIt(uint8_t *buf, uint32_t length, uint8_t endChar)
{
  if (this->txState != STATE_READY)
  {
    return RESULT_BUSY;
  }
  
  this->txState = STATE_BUSY;
  
  this->txBufferPtr = buf;
  this->txBufferLength = length;
  this->txEndChar = endChar;
  
  USART_TXEIE_SET;
  
  return RESULT_OK;
}

/**
  * @brief  Start non-blocking transmission using DMA.
  * @param  buf         Buffer address
  * @param  length      Length of Buffer
  * @param  endChar     End of data character
  * @retval UsartResult
  */
UsartResult My_USART::transmitDma(uint8_t *buf, uint32_t length, uint8_t endChar)
{
  if (this->txState != STATE_READY)
  {
    return RESULT_BUSY;
  }
  
  if (this->uart->dmaTx.dma == 0)
  {
    return RESULT_NOT_SUPPORTED;
  }
  
  this->txState = STATE_BUSY;
  
  for (uint32_t i = 0; i < length; i += 1)
  {
    if (*(buf + i) == endChar)
    {
      length = i;
    }
  }
  
  this->uart->dmaTx.dmaChannel->CPAR = (uint32_t) &(this->uart->instance->TDR);    // periphiral address
  this->uart->dmaTx.dmaChannel->CMAR = (uint32_t) buf;                   // memory address        
  this->uart->dmaTx.dmaChannel->CNDTR = length;                          // data length
  this->uart->dmaTx.dmaChannel->CCR = (uint32_t) (DMA_CCR_MINC | DMA_CCR_DIR);   // direction, circular, address increment mode, memory data size, interrupts
  SET_BIT(this->uart->dmaTx.dmaChannel->CCR, DMA_CCR_EN);
  
  SET_BIT(this->uart->instance->CR3, USART_CR3_DMAT);
  
  return RESULT_OK;
}

/**
  * @brief  Start blocking receiving.
  * @param  buf         Buffer address
  * @param  length      Length of Buffer
  * @param  endChar     End of data character
  * @param  timeout     Timeout from the last received character
  * @retval UsartResult
  */
UsartResult My_USART::receive(uint8_t *buf, uint32_t length, uint8_t endChar, uint32_t timeout)
{
  if (this->rxState != STATE_READY)
  {
    return RESULT_BUSY;
  }
  
  this->timeoutValidate(&timeout);
  
  this->rxState = STATE_BUSY;
  
  this->rxBufferPtr = buf;
  this->rxBufferLength = length;
  this->rxEndChar = endChar;
  
  UsartResult result = RESULT_OK;
  
  this->receiveStartTime = *(this->timeoutCounter);
  
  while (result == RESULT_OK)
  {
    result = waitFlagTimeout(USART_ISR_RXNE, SET, timeout);
    
    if (result != RESULT_OK)
    {
      this->rxState = STATE_READY;
      return result;
    }
    
    result = this->receiveData();
  }
  
  this->rxState = STATE_READY;
  
  return RESULT_OK;
}

/**
  * @brief  Start non-blocking receiving using DMA.
  * @param  buf         Buffer address
  * @param  length      Length of Buffer
  * @param  endChar     End of data character
  * @param  circ        Circular mode
  * @retval UsartResult
  */
UsartResult My_USART::receiveDma(uint8_t *buf, uint32_t length, uint8_t endChar, bool circ)
{
  if (this->rxState != STATE_READY)
  {
    return RESULT_BUSY;
  }
  
  if (this->uart->dmaRx.dma == 0)
  {
    return RESULT_NOT_SUPPORTED;
  }
  
  this->rxState = STATE_BUSY;
  
  this->rxBufferLength = length;
  
  this->uart->dmaRx.dmaChannel->CPAR = (uint32_t) &(this->uart->instance->RDR);    // periphiral address
  this->uart->dmaRx.dmaChannel->CMAR = (uint32_t) buf;                   // memory address        
  this->uart->dmaRx.dmaChannel->CNDTR = length;                          // data length
  
  uint32_t temp = DMA_CCR_MINC | DMA_CCR_TCIE;
  if (circ)
  {
    circCounter = 0;
    temp |= DMA_CCR_CIRC;
  }
  this->uart->dmaRx.dmaChannel->CCR = temp;  // circular, address increment mode, interrupt
  
  SET_BIT(this->uart->dmaRx.dmaChannel->CCR, DMA_CCR_EN);
  
  SET_BIT(this->uart->instance->CR3, USART_CR3_DMAR); // DMA request enable
  
  return RESULT_OK;
}

/**
  * @brief  Return next character in receiving buffer (for circular mode).
  * @param  symb        1-byte buffer
  * @retval UsartResult
  */
UsartResult My_USART::nextReceivedByte(uint8_t *symb)
{
  if (this->rxState == STATE_READY)
  {
    return RESULT_STOP;
  }
  
  if (circCounter == (this->rxBufferLength - this->uart->dmaRx.dmaChannel->CNDTR))
  {
    return RESULT_BUSY;
  }
  
  *symb = this->rxBufferPtr[circCounter];
  
  if (circCounter == this->rxBufferLength - 1)
  {
    circCounter = 0;
  }
  else
  {
    circCounter += 1;
  }
  
  return RESULT_OK;
}

/**
  * @brief  Start non-blocking receiving using RXNE interrupt.
  * @param  buf         Buffer address
  * @param  length      Length of Buffer
  * @param  endChar     End of data character
  * @retval UsartResult
  */
UsartResult My_USART::receiveIt(uint8_t *buf, uint32_t length, uint8_t endChar)
{
  if (this->rxState != STATE_READY)
  {
    return RESULT_BUSY;
  }
  
  this->rxState = STATE_BUSY;
  
  this->rxBufferPtr = buf;
  this->rxBufferLength = length;
  this->rxEndChar = endChar;
  
  USART_RXNEIE_SET;
  
  return RESULT_OK;
}

/**
  * @brief  Interrupt handler for non-block operations using RXNE and TXE. 
  *         Must be called in the corresponding interrupt vector.
  * @retval None
  */
void My_USART::interruptHandler(void)
{
  if (this->txBufferLength != 0)
  {
    if ((this->uart->instance->ISR & USART_ISR_TXE) == USART_ISR_TXE)
    {
      UsartResult result = this->transmitData();
      
      if (result == RESULT_STOP)
      {
        USART_TXEIE_RESET;
        this->txState = STATE_READY;
      }
    }
  }
  
  if (this->rxBufferLength != 0)
  {
    if ((this->uart->instance->ISR & USART_ISR_RXNE) == USART_ISR_RXNE)
    {
      UsartResult result = this->receiveData();
      
      if (result == RESULT_STOP)
      {
        USART_RXNEIE_RESET;
        this->rxState = STATE_READY;
      }
    }
  }
}
